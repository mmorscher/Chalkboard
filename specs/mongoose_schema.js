var UserSchema = new Schema({
    FirstName: String,
    LastName:  String,
    UserName:  String,
    Password:  String,
    Email:     String
});

var ChalkBoard = new Schema({
    BoardName:    String,
    BoardOwner:   Schema.ObjectId,
    LastModified: Date,
    Lines:        [ChalkLine],
    Texts:        [ChalkText]
});

var ChalkLine = new Schema({
    Color:  String,
    Width:  Number,
    Points: [ChalkPoint]
});

var ChalkPoint = new Schema({
    X:        Number,
    Y:        Number,
    Pressure: Number
});

var ChalkText = new Schema({
    X:      Number,
    Y:      Number,
    Height: Number,
    Width:  Number,
    Text:   String
})