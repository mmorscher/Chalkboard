var express = require('express'),
  mongoose = require('mongoose');
var router = express.Router();

var ModelBoard = mongoose.model('ChalkBoard'),
  ModelUser = mongoose.model('User');

/* GET users listing. */
router.route('/')
  .get(function (req, res, next) {
    mongoose.model('User').find({}, function (err, users) {
      if (err) {
        return console.log(err); // CONSIDER: Might want to call next with error.  can add status code and error message.
      } else {
        res.format({
          json: function () {
            res.json(users);
          }
        });
      }
    });
  })
  .post(function (req, res) {
    var User = mongoose.model('User');
    var user = new User(req.body).save(function (err, nuser) {
      if (err) {
        res.status(500);
        res.send('Problem adding contact to db.'); // CONSIDER: Might want to call next with error.  can add status code and error message.
      } else {
        res.status(200);
        res.format({
          json: function () {
            res.json(nuser);
          }
        });
      }
    });
  });

// route middleware to validata :id
router.param('userid', function (req, res, next, id) {
  ModelUser.findById(id, function (err, user) {
    if (err || user === null) {
      res.status(404);
      err = new Error('Not Found');
      err.status = 404;
      res.format({
        json: function () {
          res.json({ message: err.status + ' ' + err });
        }
      });
    } else {
      // once validation is done, save new id in the req
      req.userid = id;
      next();
    }
  });
});

// route middleware to validata :id
router.param('boardid', function (req, res, next, id) {
  req.boardid = id;
  next();
});

router.route('/:userid')
  .get(function (req, res) {
    ModelUser.findById(req.userid)
      .exec(
      function (err, user) {
        if (err) {
          handleError(404, err, res, 'Problem finding user');
        } else {
          res.status(200);
          res.format({
            json: function () {
              res.json(user);
            }
          });
        }
      });
  });

router.route('/:userid/boards')
  .get(function (req, res) {
    ModelUser.findById(req.userid)
      .populate('MyBoards')
      .exec(
      function (err, user) {
        if (err) {
          handleError(404, err, res, 'Problem finding user');
        } else {
          res.status(200);
          res.format({
            json: function () {
              res.json(user.MyBoards);
            }
          });
        }
      });
  })
  .post(function (req, res) {
    new ModelBoard(req.body).save(function (err, board) {
      if (err) {
        handleError(500, err, res, 'Problem creating board');
      } else {
        ModelUser.findByIdAndUpdate(req.userid, { $push: { "MyBoards": board._id } }, {})
          .exec(
          function (err1, user) {
            if (err1) {
              handleError(500, err1, res, 'Problem creating board');
            } else {
              res.status(200);
              res.format({
                json: function () {
                  res.json(board);
                }
              });
            }
          });
      }
    });
  });

router.route('/:userid/boards/:boardid')
  .delete(function (req, res) {
    ModelUser.findById(req.userid, function (err, user) {
      if (err) {
        handleError(404, err, res, 'Problem finding user');
      } else {
        user.MyBoards.remove(req.boardid);
        user.save();
        ModelBoard.findByIdAndRemove(req.boardid).exec();
        res.status(200);
        res.format({
          json: function () {
            res.json({ 'message': 'deleted' });
          }
        });
      }
    });
  });


function handleError(code, err, res, msg) {
  res.status(code);
  err = new Error(msg);
  err.status = code;
  res.format({
    json: function () {
      res.json({ message: err.status + ' ' + err });
    }
  });
}




module.exports = router;
