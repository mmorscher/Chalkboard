var express = require('express'),
  mongoose = require('mongoose');
var router = express.Router();

var ModelBoard = mongoose.model('ChalkBoard'),
  ModelLine = mongoose.model('ChalkLine');

/* GET users listing. */
router.route('/')
  .get(function (req, res, next) {
    ModelBoard.find({}, function (err, boards) {
      if (err) {
        return console.log(err); // CONSIDER: Might want to call next with error.  can add status code and error message.
      } else {
        res.status(200);
        res.format({
          json: function () {
            res.json(boards);
          }
        });
      }
    });
  })
  .post(function (req, res) {
    var board = new ModelBoard(req.body).save(function (err, nboard) {
      if (err) {
        res.status(500);
        res.send('Problem adding board to db.'); // CONSIDER: Might want to call next with error.  can add status code and error message.
      } else {
        res.status(200);
        res.format({
          json: function () {
            res.json(nboard);
          }
        });
      }
    });
  });

// route middleware to validata :id
router.param('boardid', function (req, res, next, id) {
  ModelBoard.findById(id, function (err, board) {
    if (err || board === null) {
      res.status(404);
      err = new Error('Not Found');
      err.status = 404;
      res.format({
        // html: function(){
        //     next(err);
        // },
        json: function () {
          res.json({ message: err.status + ' ' + err });
        }
      });
    } else {
      // once validation is done, save new id in the req
      req.boardid = id;
      next();
    }
  });
});

router.route('/:boardid')
  .get(function (req, res, next) {
    ModelBoard.findById(req.boardid)
      .exec(
      function (err, board) {
        if (err) {
          handleError(err, req, 'Problem getting board with id: ' + req.boardid);
        } else {
          res.status(200);
          res.format({
            json: function () {
              res.json(board);
            }
          });
        }
      });
  })
  .put(function (req, res) {
    var board = new ModelBoard(req.body);
    delete board._id;
    ModelBoard.findByIdAndUpdate(req.boardid, board, {})
      .exec(
      function (err, board) {
        if (err) {
          handleError(err, res, 'Problem updating board with id: ' + req.boardid);
        } else {
          res.status(200);
          res.format({
            json: function () {
              res.json(board);
            }
          });
        }
      });
  })
  .delete(function (req, res) {
    ModelBoard.findByIdAndRemove(req.boardid)
      .exec(
      function (err, board) {
        if (err) {
          handleError(err, res, 'Problem deleting contact');
        } else {
          res.status(204);
          res.format({
            json: function () {
              res.json(null);
            }
          });
        }
      }
      );
  });

router.route('/:boardid/lines')
  .get(function (req, res) {
    ModelBoard.findById(req.boardid)
      .populate('Lines')
      .exec(function (err, board) {
        if (err) {
          handleError(err, res);
        }
        else {
          res.status(200);
          res.format({
            json: function () {
              console.log(board);
              res.json(board);
            }
          });
        }
      });
  })
  .post(function (req, res) {
    ModelLine(req.body).save(function (err, line) {
      if (err) {
        console.log(err);
      }
      else {
        ModelBoard.findByIdAndUpdate(
          req.boardid,
          { $push: { "Lines": line._id } },
          { safe: true, upsert: true },
          function (err, board) {
            if (err) {
              handleError(err, res);
            }
            else {
              res.status(200);
              res.format({
                json: function () {
                  res.json(line);
                }
              });
            }
          }
        );
      }
    });
  })
  .delete(function (req, res) {
    ModelBoard.findById(req.boardid,
      function (err, board) {
        if (err) {
          handleError(err, res);
        }
        else {
          var lines = req.body.Lines;

          if (lines === undefined) {
            board.Lines = [];
            board.save(function (err, nboard) {
              if (err) {
                handleError(err, res);
              }
              else {
                res.status(204);
                res.send();
              }
            });
          }
          else {
            for (var i = 0; i < lines.length; i++) {
              board.Lines.remove(lines[i]);
            }
            board.save(function (err, nboard) {
              if (err) {
                handleError(err, res);
              }
              else {
                res.status(204);
                res.send();
              }
            });

          }

        }
      });
  });

// route middleware to validata :id
router.param('lineid', function (req, res, next, id) {
  req.lineid = id;
  next();
});

router.route('/:boardid/lines/:lineid')
  .get(function (req, res) {
    ModelLine.findById(req.lineid)
      .exec(function (err, line) {
        if (err) {
          handleError(err, res);
        }
        else {
          res.status(200);
          res.format({
            json: function () {
              console.log(line);
              res.json(line);
            }
          });
        }
      });
  })
  .post(function (req, res) {

  })
  .put(function (req, res) {

  })
  .delete(function (req, res) {
    deleteLine(res, req.boardid, req.lineid);
  });

function deleteLine(res, boardid, lineid) {
  ModelBoard.findById(boardid,
    function (err, board) {
      if (err) {
        handleError(err, res);
      }
      else {
        for (var i = 0; i < board.Lines.length; i++) {
          if (String(board.Lines[i]) == String(lineid)) {
            board.Lines.remove(lineid);
            board.save(function (err) {
              if (err) {
                handleError(err, res);
              }
              else {
                console.log('deleted from board');
                ModelLine.findByIdAndRemove(lineid, function (err, line) {
                  if (err) {
                    handleError(err, res);
                  }
                  else {
                    res.status(204);
                    res.format({
                      json: function () {
                        res.json({ message: 'deleted' });
                      }
                    });
                  }
                });
              }
            });
          }
        }

      }
    }, false, true);
}

function handleError(err, res, msg) {
  res.status(404);
  err = new Error(msg);
  err.status = 404;
  res.format({
    json: function () {
      res.json({ message: err.status + ' ' + err });
    }
  });
}



module.exports = router;
