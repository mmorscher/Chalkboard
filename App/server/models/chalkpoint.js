var mongoose = require('mongoose');

var chalkPointSchema = new mongoose.Schema({
    X: Number,
    Y: Number,
    Pressure: Number
});

mongoose.model('ChalkPoint', chalkPointSchema);