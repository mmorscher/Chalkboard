var mongoose = require('mongoose');

var chalkTextSchema = new mongoose.Schema({
    X: Number,
    Y: Number,
    Size: Number,
    Color: String,
    Font: String,
    Text: String
});

mongoose.model('ChalkText', chalkTextSchema);