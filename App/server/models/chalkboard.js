var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var chalkBoardSchema = new Schema({
    BoardName: String,
    BoardOwner: {type: ObjectId, ref: 'User'},
    LastModified: Date,
    Lines: [{type: ObjectId, ref: 'ChalkLine'}],
    Texts: [{type: ObjectId, ref: 'ChalkText'}]
});

mongoose.model('ChalkBoard', chalkBoardSchema);