var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var chalkLineSchema = new mongoose.Schema({
    Color: String,
    Stroke: Number,
    InitPoint: {
        X: Number,
        Y: Number,
        Pressure: Number
    },
    NextPoint: {
        X: Number,
        Y: Number,
        Pressure: Number
    }
});

mongoose.model('ChalkLine', chalkLineSchema);