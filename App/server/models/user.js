var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var userSchema = new mongoose.Schema({
    FirstName: String,
    LastName: String,
    UserName: String,
    Password: String,
    Email: String,
    MyBoards: [{type: ObjectId, ref: 'ChalkBoard'}]
});

mongoose.model('User', userSchema);