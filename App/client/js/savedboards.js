var apiUrl = "http://localhost:4500/";
var userId = "5821554c3487834ef06fdde7";
var boards = [];
var boardContainer = $('#board-container');

var modal = document.getElementById('myModal');
var modalclose = document.getElementsByClassName("close")[0];

function init() {
    console.log('hello world');
    getBoards();
}

function storeBoardAndDraw(boardId) {
    var error = false;
    try {
        console.log("saving boardId: " + boardId);
        sessionStorage.setItem("boardIdToStore", boardId);
    } catch (e) {
        alert("Error saving to session storage");
        error = true;
    }

    if (!error) {
        window.location = './draw.html';
    }
}

function displayBoards() {
    boardContainer.html("");
    for (var i = 0; i < boards.length; i++) {
        var tempBoard = $('<div></div>', {
            "class": 'flex-item board',
            "id": boards[i]._id
        });
        tempBoard.appendTo(boardContainer);
        var boardName = boards[i].BoardName;
        $('<span class="close">x</span>').appendTo(tempBoard)
            .bind('click', function (e) {
                e.stopPropagation();
                console.log('delete board: ' + e.currentTarget.parentElement.id);
                deleteBoard(e.currentTarget.parentElement.id);
            });
        $("<p></p>").text(boardName).appendTo(tempBoard);

        tempBoard.bind('click', function (e) {
            console.log(e.currentTarget);
         storeBoardAndDraw(e.currentTarget.id);
        });
    }
    addNewBoard();
}

function addNewBoard() {
    var newBoard = $('<div></div>', {
        "class": 'flex-item newboard'
    });
    newBoard.appendTo(boardContainer);
    var boardName = "New Board";
    $("<p></p>").text(boardName).appendTo(newBoard);
    $('<i class="fa fa-plus fa-2x" aria-hidden="true"></i>').appendTo(newBoard);



    newBoard.bind('click', function (e) {
        console.log('add new board');
        modal.style.display = "block";
    });

    modalclose.onclick = function () {
        modal.style.display = "none";
    }

    window.addEventListener('click', function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    });

    $("form").submit(function () {
        event.preventDefault();
        var newboard = $('form').serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        newboard.BoardOwner = userId;
        console.log(newBoard);
        $.ajax({
            url: apiUrl + 'users/' + userId + '/boards',
            type: 'POST',
            dataType: 'JSON',
            data: newboard,
            success: function (data) {
                console.log(data);
                getBoards();
            }
        });
    });
}

function deleteBoard(boardId) {
    $.ajax({
        url: apiUrl + 'users/' + userId + '/boards/' + boardId,
        type: 'DELETE',
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            getBoards();
        }
    });
}

function getBoards() {
    $.ajax({
        url: apiUrl + 'users/' + userId + '/boards',
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            boards = data;
            displayBoards();
        }
    });
}

init();