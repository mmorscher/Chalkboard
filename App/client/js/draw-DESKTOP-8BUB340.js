var initX = 0,
    initY = 0,
    nextX = 0,
    nextY = 0;
var height, width;
var canvas, context;
var drawFlag = false;
var allPoints = [];
var currPoints = [];
var drawingTool = "pencil";
var color = "white";
var linesToDelete = [];

var apiUrl = "http://localhost:4500/boards/";
var boardId = "";
var board = {};

function init() {
    canvas = $('.canvas');
    context = canvas[0].getContext('2d');
    var winHeight = $(window).height() - $('#header-bar').height() - $('.draw-bar').height() - 10;
    var winWidth = $(window).width() - 10;
    context.canvas.height = winHeight;
    context.canvas.width = winWidth;
    loadBoard();

    canvas.mousedown(function (o) {
        getPos("down", o);
    });
    canvas.mousemove(function (o) {
        getPos("move", o);
    });
    canvas.mouseup(function (o) {
        getPos("up", o);
    });
    canvas.mouseout(function (o) {
        getPos("out", o);
    });

    $('.trash').on('click', trash);
    $('.undo').on('click', undo);
    $('#pencil').on('click', drawTool);
    $('#eraser').on('click', drawTool);
    $('#color').on('click', changeColor);
    $('#dropdown').on('change', colors);
    if ($('.white').on('click', function () {
        color = "white";
        colors();
    }));
    if ($('.red').on('click', function () {
        color = "red";
        colors();
    }));
    if ($('.blue').on('click', function () {
        color = "blue";
        colors();
    }));
    if ($('.green').on('click', function () {
        color = "green";
        colors();
    }));
    if ($('.orange').on('click', function () {
        color = "orange";
        colors();
    }));
    if ($('.yellow').on('click', function () {
        color = "yellow";
        colors();
    }));
    if ($('.black').on('click', function () {
        color = "black";
        colors();
    }));

    (function poll() {
        setTimeout(function () {
            $.ajax({
                url: apiUrl + boardId + "/lines/",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    if (data) {
                        board = data;
                        drawBoard(board);
                        console.log('updated board');
                        poll();
                    }
                    else {
                        // console.log("Board not found");
                    }
                }
            });
        }, 200);
    })();
}

function getPos(type, o) {
    var pos = getMousePos(canvas[0], o);
    var chalkPoint = {
        x: 0,
        y: 0,
        color: color,
        lineThickness: "4"
    };
    if (type == "down") {
        drawFlag = true;
        initX = nextX;
        initY = nextY;
        nextX = pos.x;
        nextY = pos.y;
        chalkPoint.x = nextX;
        chalkPoint.y = nextY;
        currPoints.push(chalkPoint);
        context.beginPath();
        if (drawingTool == "pencil") {
            chalkPoint.color = color;
            chalkPoint.lineThickness = "4";
        } else {
            chalkPoint.color = "black";
            chalkPoint.lineThickness = "20";
        }
        context.fillStyle = chalkPoint.color;
        context.fillRect(nextX, nextY, 2, 2);
        context.closePath();
    }

    if (type == "move") {
        if (drawFlag) {
            initX = nextX;
            initY = nextY;
            nextX = pos.x;
            nextY = pos.y;
            chalkPoint.x = nextX;
            chalkPoint.y = nextY;
            currPoints.push(chalkPoint);
            if (drawingTool == "pencil") {
                chalkPoint.color = color;
                chalkPoint.lineThickness = "4";
                draw(chalkPoint.color, chalkPoint.lineThickness);
            } else {
                erase(nextX, nextY);
            }
        }
    }
    if (type == "up" || type == "out") {
        if (currPoints.length > 0 && drawingTool == "pencil") {
            allPoints.push(currPoints);
        }
        currPoints = [];
        drawFlag = false;
        if (linesToDelete.length > 0) {
            deleteLines(linesToDelete);
            linesToDelete = [];
        }
    }

}

function erase(x, y) {
    for (var i = 0; i < board.Lines.length; i++) {
        var line = board.Lines[i];
        var line_x_init = Math.floor(line.InitPoint.X);
        var line_y_init = Math.floor(line.InitPoint.Y);
        var line_x_next = Math.floor(line.InitPoint.X);
        var line_y_next = Math.floor(line.InitPoint.Y);
        if ((Math.floor(x) > line_x_init - 10) && (Math.floor(x) < line_x_init + 10) && (Math.floor(x) > line_x_next - 10) && (Math.floor(x) < line_x_next + 10)) {
            if ((Math.floor(y) > line_y_init - 10) && (Math.floor(y) < line_y_init + 10) && (Math.floor(y) > line_y_next - 10) && (Math.floor(y) < line_y_next + 10)) {
                board.Lines.splice(i, 1);
                linesToDelete.push(line._id);
                redrawBoard(board);
            }
        }
    }
}

function deleteLines(lines) {
    var json = { "Lines": lines };
    $.ajax({
        url: apiUrl + board._id + '/lines',
        type: 'DELETE',
        dataType: 'JSON',
        data: json,
        success: function (data) {
            console.log('deleted ' + lines.length + ' lines');
        }
    });
}

function draw(color, lineThickness) {
    drawLine(initX, initY, nextX, nextY, color, lineThickness);
    saveLine(initX, initY, nextX, nextY, color, lineThickness);
}

function drawLine(x1, y1, x2, y2, color, lineThickness) {
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.strokeStyle = color;
    context.lineWidth = lineThickness;
    context.stroke();
    context.closePath();
}

function redrawBoard(board) {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    for (var i = 0; i < board.Lines.length; i++) {
        context.beginPath();
        var line = board.Lines[i];
        context.moveTo(line.InitPoint.X, line.InitPoint.Y);
        context.lineTo(line.NextPoint.X, line.NextPoint.Y);
        context.strokeStyle = line.Color;
        context.lineWidth = "4";
        context.stroke();
        context.closePath();
    }
}

function changeColor() {
    var dropdown = $('#dropdown');
    if (dropdown.hasClass('dropdown-hide')) {
        dropdown.removeClass('dropdown-hide');
        dropdown.addClass('dropdown-show');
    } else {
        dropdown.removeClass('dropdown-show');
        dropdown.addClass('dropdown-hide');
    }
}

function colors() {
    $('#dropdown').removeClass('dropdown-show');
    $('#dropdown').addClass('dropdown-hide');
    $('.fa-eyedropper').css('color', color);
}

function trash() {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    allPoints = [];
    $.ajax({
        url: apiUrl + board._id + '/lines',
        type: 'DELETE',
        dataType: 'JSON',
        success: function () {
            board.Lines = [];
            redrawBoard(board);
        }
    });
}

function undo() {
    if (allPoints.length > 0) {
        var undoPoints = allPoints.pop();
        redraw(allPoints);
    }
}

function drawTool() {
    var pencil = $('#pencil');
    var eraser = $('#eraser');
    if (pencil.hasClass('pencil-show')) {
        pencil.removeClass('pencil-show');
        pencil.addClass('pencil-hide');
        eraser.removeClass('eraser-hide');
        eraser.addClass('eraser-show');
        drawingTool = "eraser"
    } else {
        eraser.removeClass('eraser-show');
        eraser.addClass('eraser-hide');
        pencil.removeClass('pencil-hide');
        pencil.addClass('pencil-show');
        drawingTool = "pencil";
    }
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect(), // abs. size of element
        scaleX = canvas.width / rect.width, // relationship bitmap vs. element for X
        scaleY = canvas.height / rect.height; // relationship bitmap vs. element for Y
    return {
        x: (evt.clientX - rect.left) * scaleX, // scale mouse coordinates after they have
        y: (evt.clientY - rect.top) * scaleY // been adjusted to be relative to element
    }
}

function drawBoard(board) {
    $("title").text(board.BoardName);
    $(".header-header").text(board.BoardName);
    var lines = board.Lines;
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        drawLine(line.InitPoint.X, line.InitPoint.Y, line.NextPoint.X, line.NextPoint.Y, line.Color, line.Stroke);
    }
}

function getBoard() {
    $.ajax({
        url: apiUrl + boardId + "/lines/",
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            if (data) {
                board = data;
                drawBoard(board);
                console.log('updated board');
            }
            else {
                // console.log("Board not found");
            }
        }
    });
}

function loadBoard() {
    var error = false;
    try {
        boardId = sessionStorage.getItem("boardIdToStore");
        console.log("loaded boardid: " + boardId);
    } catch (e) {
        alert("Error reading from session storage" + e);
        error = true;
    }

    if (!error && boardId !== undefined) {
        getBoard();
    }
}

function saveLine(initX, initY, nextX, nextY, color, lineThickness) {
    var line = {
        Color: color,
        Stroke: lineThickness,
        InitPoint: {
            X: initX,
            Y: initY
        },
        NextPoint: {
            X: nextX,
            Y: nextY
        }
    }
    // console.log(JSON.stringify(line));
    $.ajax({
        url: apiUrl + boardId + '/lines',
        type: 'POST',
        dataType: 'JSON',
        data: line,
        success: function (line) {
            if (line) {
                board.Lines.push(line);
                // console.log(line);
            }
            else {
                // console.log("board not found");
            }
        }
    });
}

function deleteLine(line) {
    $.ajax({
        url: apiUrl + boardId + '/lines/' + line._id,
        type: 'DELETE',
        dataType: 'JSON'
    });
}

$(window).on('load', init);