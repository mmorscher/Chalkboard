var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: './',
            index: './html/login.html'
        }
    });
});

gulp.task('css', function(){
    return gulp.src('css/*.css')
        .pipe(browserSync.stream());
});

gulp.task('watch', ['browserSync'], function(){
    gulp.watch('css/*.css', ['css']);
    gulp.watch('js/*.js', browserSync.reload);
    gulp.watch('html/*.html', browserSync.reload);
});